# Gitlab Pipeline [![pipeline status](https://gitlab.com/araulet-team/devops/gitlab-ci-group/docker-pipeline/badges/master/pipeline.svg)](https://gitlab.com/araulet-team/devops/gitlab-ci-group/docker-pipeline/commits/master) [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Defaut pipeline for **Dockerize** projects not related to any programming language. (This example use a Nodejs but the flow stay the same whatever programming language you want to use)

## Features

#### Default inherited from `classic-pipeline` project

* Generate **CHANGELOG**, **release**, **pre_release** etc. notes
* Tagging branches when new release is created
* Publish Docker images to registry
* Deploy to specific environments
* Building optimized images (e.g. with nodejs project)
* Execute code quality job

## Stages

| # | stage | description |
| - | ----- | ----------- |
| 1 | **prepare**       | create `build_info` and `.next-version` files containing metadata + update `CHANGELOG.md`. Store everything in Artifact so that it's available for next stages |
| 2 | **build**         | build Docker images for development and production and store them in artefacts (to be reuse in others jobs) | 
| 3 | **codequality**   | code quality jobs (tests, security etc.) |
| 4 | **publish**       | publish docker image to registry |
| 5 | **release**       | (**depends on publish**) commit new Gitlab release + notes + git tag |
| 6 | **deploy**        | (**depends on release**) deploy to the specific environment |

## Flow

Because `develop` and `master` generate new commits on release, we need a way to maintain these branches in sync with a strict git flow. (inspired by https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/)

##### Feature branches

| # | action | note |
| - | ------ | ---- |
| 1 | `feature/*` | create a feature branch from `develop` |
| 2 | `feature/*` to `develop` | merge your `feature/` branch to `develop` |
| 3 | `develop` | generate Gitlab release + `pre_release_info` notes + tag with a new commit |
| 4 | `develop` to `master` | merge `develop` to `master` |
| 5 | `master` | generate Gitlab release + `release_info` notes + tag with a new commit |
| 6 | `master` to `develop` | add `[skip-ci]` to not trigger another release |

##### Hotfix branches

| # | action | note |
| - | ------ | ---- |
| 1 | `hotfix/*` | create a hotfix branch from `master` |
| 2 | `hotfix/*` to `master` | merge your `hotfix/` branch to `master` |
| 3 | `master` | generate Gitlab release + `release_info` notes + tag with a new commit |
| 4 | `master` to `develop` | merge `master` to `develop` |
| 5 | `develop` | generate Gitlab release + `pre_release_info` notes + tag with a new commit |

## Configuration

Variable to be configured

| # | name | description |
| - | ---- | ----------- |
| 1 | **RELEASE_DESC** | description for your release |
